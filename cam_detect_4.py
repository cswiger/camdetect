#!/usr/bin/env python3
# a combo of scripts from
# https://github.com/google-coral/tflite.git
# https://github.com/google-coral/examples-camera

from picamera import PiCamera
import time
import cv2
import io
import collections

import tflite_runtime.interpreter as tflite
from PIL import Image
from PIL import ImageDraw
import numpy as np

EDGETPU_SHARED_LIB = 'libedgetpu.so.1'

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 30


Object = collections.namedtuple('Object', ['id', 'score', 'bbox'])

class BBox(collections.namedtuple('BBox', ['xmin', 'ymin', 'xmax', 'ymax'])):
  __slots__ = ()
  @property
  def width(self):
    """Returns bounding box width."""
    return self.xmax - self.xmin
  @property
  def height(self):
    """Returns bounding box height."""
    return self.ymax - self.ymin
  @property
  def area(self):
    """Returns bound box area."""
    return self.width * self.height
  @property
  def valid(self):
    return self.width >= 0 and self.height >= 0
  def scale(self, sx, sy):
    return BBox(xmin=sx * self.xmin,
                ymin=sy * self.ymin,
                xmax=sx * self.xmax,
                ymax=sy * self.ymax)
  def translate(self, dx, dy):
    return BBox(xmin=dx + self.xmin,
                ymin=dy + self.ymin,
                xmax=dx + self.xmax,
                ymax=dy + self.ymax)
  def map(self, f):
    return BBox(xmin=f(self.xmin),
                ymin=f(self.ymin),
                xmax=f(self.xmax),
                ymax=f(self.ymax))
  @staticmethod
  def intersect(a, b):
    return BBox(xmin=max(a.xmin, b.xmin),
                ymin=max(a.ymin, b.ymin),
                xmax=min(a.xmax, b.xmax),
                ymax=min(a.ymax, b.ymax))
  @staticmethod
  def union(a, b):
    return BBox(xmin=min(a.xmin, b.xmin),
                ymin=min(a.ymin, b.ymin),
                xmax=max(a.xmax, b.xmax),
                ymax=max(a.ymax, b.ymax))
  @staticmethod
  def iou(a, b):
    intersection = BBox.intersect(a, b)
    if not intersection.valid:
      return 0.0
    area = intersection.area
    return area / (a.area + b.area - area)

def make(i):
  ymin, xmin, ymax, xmax = boxes[i]
  return Object(
      id=int(class_ids[i]),
      score=float(scores[i]),
      bbox=BBox(xmin=xmin,
                ymin=ymin,
                xmax=xmax,
                ymax=ymax).scale(sx, sy).map(int))

def draw_objects(draw, objs, labels):
  """Draws the bounding box and label for each object."""
  for obj in objs:
    bbox = obj.bbox
    draw.rectangle([(bbox.xmin, bbox.ymin), (bbox.xmax, bbox.ymax)],
                   outline='white')
    draw.text((bbox.xmin + 10, bbox.ymin + 10),
              '%s\n%.2f' % (labels.get(obj.id, obj.id), obj.score),
              fill='white')


# load labels
with open('/home/pi/ml/coral/examples-camera/all_models/coco_labels.txt', 'r', encoding='utf-8') as f:
   lines = f.readlines()
   if not lines:
      labels = {}
   if lines[0].split(' ', maxsplit=1)[0].isdigit():
      pairs = [line.split(' ', maxsplit=1) for line in lines]
      labels = {int(index): label.strip() for index, label in pairs}
   else:
      labels = {index: line.strip() for index, line in enumerate(lines)}

# load model - this script is hard coded to expect a model with input tensor size 300x300 hence it is NOT a nice
# easy to change command line arg 
interpreter = tflite.Interpreter(model_path='/home/pi/ml/coral/examples-camera/all_models/mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite',
	experimental_delegates=[ tflite.load_delegate(EDGETPU_SHARED_LIB,
		{})
	])

interpreter.allocate_tensors()

stream = io.BytesIO()
threshold = 0.4

# for old function detect.set_input
# get input size of model - model used is 300x300
_, width, height, _ = interpreter.get_input_details()[0]['shape']
# formerly from camera.resolution, that is now resized in gpu to near TPU size
w,h = (320,240) 
# make it fit the model input tensor - scale = 0.9375
scale = min(width / w, height / h)
# these end up w2=300, h2=225
w2, h2 = int(w * scale), int(h * scale)
tensor_index = interpreter.get_input_details()[0]['index']
tensorinp = interpreter.tensor(tensor_index)()[0]
_, _, channel = tensorinp.shape
# these reference have to be removed or interpreter throws an error - this is not an issue when the
# helpers are functions with temporary objects that automagically go away, but in my flattened script
# they have to be manually removed
del(tensorinp)

# sx=sy=320 
sx, sy = width / scale, height / scale
avgtime = 1/10.
reporttime = 0

# display the 320x240 image on a larger window for visibility
cv2.namedWindow('detect',cv2.WINDOW_NORMAL)
cv2.resizeWindow('detect',(640,480))

# 320x240 is what the camera supports, using rgb instead of jpeg now, with gpu resize
for frame in camera.capture_continuous(stream, format="rgb", use_video_port=True, resize=(320,240)):
   # to measure fps, where time only goes forward
   start = time.monotonic()
   stream.truncate()
   stream.seek(0)
   # in this array, reshape to height by width
   input = np.frombuffer(stream.getvalue(), dtype=np.uint8).reshape((240,320,3))
   # get the input tensor
   tensorinp = interpreter.tensor(tensor_index)()[0]
   # and zero fill because we are loading a different size array that does not fully overlap
   tensorinp.fill(0)
   # how to load a an image input array (320x240) into a different size model input tensor (300x300)
   #  h2=225 and w2=300  -- the rest is zero filled above
   tensorinp[:h2, :w2] = input[:h2,:w2]
   # again delete the objects to remove references that cause errors
   del(tensorinp)
   # go inference
   interpreter.invoke()

   # getting results
   tensorout = interpreter.tensor(interpreter.get_output_details()[0]['index'])()
   boxes = np.squeeze(tensorout)
   tensorout = interpreter.tensor(interpreter.get_output_details()[1]['index'])()
   class_ids = np.squeeze(tensorout)
   tensorout = interpreter.tensor(interpreter.get_output_details()[2]['index'])()
   scores =  np.squeeze(tensorout)
   tensorout = interpreter.tensor(interpreter.get_output_details()[3]['index'])()
   count = int(np.squeeze(tensorout))

   # get those object above the threshold set at 0.4 above
   objs = [make(i) for i in range(count) if scores[i] >= threshold]
   # again delete the reference objects to the interpreter
   del(boxes,class_ids,scores,count,tensorout)

   # I guess we have to convert it from an array to an image to draw the boxes
   image = Image.fromarray(input)
   draw_objects(ImageDraw.Draw(image), objs, labels)
   # lastly display it on the 640x480 named window created above - now we have to convert it back to an array
   # would likely be even faster if not for these conversions back and forth from array to PIL image to array
   # also need to correct the color pallet order
   cv2.imshow("detect", cv2.cvtColor(np.asarray(image), cv2.COLOR_BGR2RGB))
   # display and exit on hitting the 'q' key
   key = cv2.waitKey(1) & 0xFF
   if key == ord("q"):
     break
   # calculate and print the average fps we are getting
   avgtime += (time.monotonic() - start)
   avgtime /= 2.
   reporttime += 1
   if (reporttime > 10):
      reporttime = 0
      print("FPS: %f" % (1./avgtime))


