cam_detect_4.py -  script that uses the coral.ai TPU usb accelerator and the Mobilenet v2 SSD model to identify objects in a Raspberry Pi 2B picamera at a frame rate of around 8 FPS. 
<br/>
This is a mashup of scripts found at:<br/>
	https://github.com/google-coral/tflite.git<br/>
which identifies objects in images with bounding boxes, hacked to use the picamera but ran very slow, about 2 FPS, and<br/>
	https://github.com/google-coral/examples-camera<br/>
which ran very fast, 10-15 FPS but only displays name of objects detected on the picamera preview window with annotations.<br/>
<br/>
They were also flattened into a single script file to assist with understanding how they work, expecially the image
handling to fit the model input tensor. This one is hard coded to use a tflite quantized 8bit model of input size 300x300.
Specifically:  mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite
and the labels: coco_labels.txt
which does a pretty good job of identifying 89 objects from toothbrushes to giraffes. 
<br/>
Needs opencv cv2 installed, Tensorflow Lite and the TPU support packages

